/* hostproc - SNMP agent for monitoring detailed list of running processes 
   Copyright (C) 2020 Sergey Poznyakoff

   Hostproc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Hostproc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with hostproc. If not, see <http://www.gnu.org/licenses/>. */
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <errno.h>
#include <setjmp.h>
#include <limits.h>
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include "hostproc.h"
#include "json.h"

static void
fconfig_perror(char const *fmt, ...)
{
	va_list ap;
	char msg[512];
	va_start(ap, fmt);
	vsnprintf(msg, sizeof(msg), fmt, ap);
	va_end(ap);
	config_perror(msg);
}

struct tokbuf {
	char const *val;
	char const *cur;
	char *tokptr;
	size_t toksize;
	int reuse;
};

static void
tokbuf_init(struct tokbuf *tb, char const *val)
{
	tb->val = tb->cur = val;
	tb->tokptr = NULL;
	tb->toksize = 0;
	tb->reuse = 0;
}

static void
tokbuf_free(struct tokbuf *tb)
{
	free(tb->tokptr);
}

static char *
tokbuf_morespace(struct tokbuf *tb, size_t len)
{
	while (len > tb->toksize) {
		char *p = a2nrealloc(tb->tokptr, &tb->toksize, 1);
		if (!p)
			abend_nomem();
		tb->tokptr = p;
	}
	return tb->tokptr;
}

static int
tokbuf_eof(struct tokbuf *tb)
{
	if (tb->reuse)
		return 0;
	tb->cur += strspn(tb->cur, " \t");
	return *tb->cur == 0;
}

static char *
getquote(struct tokbuf *tb)
{
	size_t len;
	size_t nesc = 0;
	char *ret, *p;

	tb->cur++; /* Skip initial quote character */
	/* Scan the line to determine its length */
	for (len = 0; tb->cur[len] != '"'; len++) {
		if (!tb->cur[len]) {
			config_perror("unterminated quoted string near");
			return NULL;
		}
		if (tb->cur[len] == '\\') {
			nesc++;
			len++;
			if (!tb->cur[len]) {
				config_perror("unterminated quoted string near");
				return NULL;
			}
		}
	}

	ret = tokbuf_morespace(tb, len - nesc + 1);
	p = ret;
	while (*tb->cur != '"') {
		if (*tb->cur == '\\')
			tb->cur++;
		*p++ = *tb->cur++;
	}
	*p = 0;
	tb->cur++;
	
	return ret;
}
	
static char *
getstr(struct tokbuf *tb)
{
	size_t len;
	char *ret;

	if (tb->reuse) {
		tb->reuse = 0;
		return tb->tokptr;
	}
	
	len = strspn(tb->cur, " \t");
	tb->cur += len;
	if (*tb->cur == 0) {
		config_perror("expected string, but found end of line");
		return NULL;
	}
	if (*tb->cur == '"') 
		return getquote(tb);
	len = strcspn(tb->cur, " \t");

	ret = tokbuf_morespace(tb, len + 1);
	memcpy(ret, tb->cur, len);
	ret[len] = 0;
	tb->cur += len;

	return ret;
}

static int
convnum(char *val, int issize, unsigned long *retval)
{
	unsigned long n;
	char *p;
	
	n = strtoul(val, &p, 10);
	if (n == UINT_MAX && errno == ERANGE) {
		config_perror("bad number");
		return -1;
	}
	if (*p) {
		unsigned long factor;
		if (!issize) {
			config_perror("bad number");
			return -1;
		}
		
		switch (*p) {
		case 0:
		case 'k':
		case 'K':
			factor = 1; /* Sizes are expressed in kB by default */
			break;
		case 'm':
		case 'M':
			factor = 1024UL;
			break;
		case 'g':
		case 'G':
			factor = 1024*1024UL;
			break;
		default:
			config_perror("bad number");
			return -1;
		}

		if (ULONG_MAX / factor < n) {
			config_perror("bad number");
			return -1;
		}
		n *= factor;
	}
	
	*retval = n;
	return 0;
}

static int
getnum(struct tokbuf *tb, unsigned long *retval, char const *what)
{
	unsigned long n;
	char const *input;
	char *p;
	
	if (tb->reuse) {
		input = tb->tokptr;
	} else {
		tb->cur += strspn(tb->cur, " \t");
		if (*tb->cur == 0) {
			config_perror("expected number, but found end of line");
			return -1;
		}
		input = tb->cur;
	}
	n = strtoul(input, &p, 10);
	if ((n == UINT_MAX && errno == ERANGE) ||
	    (*p && !(*p == ' ' || *p == '\t'))) {
		fconfig_perror("bad %s", what);
		tb->reuse = 0;
		return -1;
	}
	*retval = n;
	if (tb->reuse)
		tb->reuse = 0;
	else
		tb->cur = p;
	return 0;
}

static int
getnum_dfl(struct tokbuf *tb, unsigned long *retval, char const *what,
	   unsigned long dfl)
{
	if (tokbuf_eof(tb)) {
		*retval = dfl;
		return 0;
	}
	return getnum(tb, retval, what);
}

static inline void
putback(struct tokbuf *tb)
{
	tb->reuse = 1;
}

static char const *
basename(char const *str)
{
	char const *p = strrchr(str, '/');
	return p ? p + 1 : str;
}
	
static int
cmp_basename(PROCGROUP *pgr, char const *str)
{
	return strcmp(basename(pgr->pattern), basename(str));
}

static int
cmp_exact(PROCGROUP *pgr, char const *str)
{
	return strcmp(pgr->pattern, str);
}

static int
cmp_rx(PROCGROUP *pgr, char const *str)
{
	return regexec(&pgr->rx, str, 0, NULL, 0);
}

static int (*cmpfun[])(PROCGROUP *, char const *str) = {
	cmp_exact,
	cmp_basename,
	cmp_rx
};

int
procgroup_match(PROCESS *proc, PROCGROUP *pgr)
{
	char *str;

	switch (pgr->field) {
	case pf_comm:
		str = proc->comm;
		break;
	case pf_exe:
		str = proc->exe;
		break;
	case pf_cmdline:
		str = proc->cmdline;
		break;
	default:
		abort();
	}
	if (!str)
		return 1;
	return cmpfun[pgr->match](pgr, str);
}

PROCGROUP *
procgroup_find(PROCGROUP_LIST *pgrlist, char const *name)
{
	PROCGROUP *pgr;
	PROCGROUP_LIST_FOREACH(pgr, pgrlist) {
		if (strcmp(pgr->name, name) == 0)
			return pgr;
	}
	return NULL;
}

PROCGROUP *
procgroup_list_match_first(PROCESS *proc, PROCGROUP_LIST *pgrlist)
{
	PROCGROUP *ent;

	PROCGROUP_LIST_FOREACH(ent, pgrlist) {
		if (procgroup_match(proc, ent) == 0)
			return ent;
	}
	return NULL;
}

PROCGROUP *
procgroup_match_next(PROCESS *proc, PROCGROUP *ent)
{
	while (ent && (ent = PROCGROUP_NEXT(ent)) != NULL) {
		if (procgroup_match(proc, ent) == 0)
			break;
	}
	return ent;
}

PROCGROUP *
procgroup_create(char const *name)
{
	PROCGROUP *ent;

	if ((ent = calloc(1, sizeof(*ent))) == NULL
	    || (ent->name = strdup(name)) == NULL
	    || (ent->pattern = strdup(name)) == NULL)
		abend_nomem();
	
	return ent;
}

void
procgroup_free(PROCGROUP *ent)
{
	free(ent->name);
	free(ent->pattern);
	if (ent->match == match_rx)
		regfree(&ent->rx);
	free(ent);
}

void
procgroup_replace(PROCGROUP *dst, PROCGROUP *src)
{
	free(dst->name);
	dst->name = src->name;
	src->name = NULL;
		
	free(dst->pattern);
	dst->pattern = src->pattern;
	src->pattern = NULL;

	if (dst->match == match_rx)
		regfree(&dst->rx);
	dst->match = src->match;
	if (src->match == match_rx) {
		dst->rx = src->rx;
		src->match = match_exact;
	}
	
	dst->field = src->field;

	dst->count        = src->count;
	dst->cpu_inst_max = src->cpu_inst_max;
	dst->cpu_avg_sum  = src->cpu_avg_sum;
	dst->cpu_inst_sum = src->cpu_inst_sum;
	dst->vsize        = src->vsize;
	dst->rss          = src->rss;

	dst->min_count    = src->min_count;
	dst->max_count    = src->max_count;
	dst->vsize_max    = src->vsize_max;
	dst->rss_max      = src->vsize_max;
	dst->cpu_max      = src->cpu_max;

	procgroup_free(src);
}

static void
procgroup_compile_rx(PROCGROUP *pgr)
{
	int ec = regcomp(&pgr->rx, pgr->pattern, REG_EXTENDED|REG_NOSUB);
	if (ec) {
		char errbuf[512];
		regerror(ec, &pgr->rx, errbuf, sizeof(errbuf));
		config_perror(errbuf);
		pgr->match = match_exact;
	}
}

static void
procgroup_set_match(PROCGROUP *pgr, int match)
{
	if (pgr->match != match) {
		if (pgr->match == match_rx)
			regfree(&pgr->rx);
		pgr->match = match;
		if (pgr->match == match_rx)
			procgroup_compile_rx(pgr);
	}
}

static int skip_group; /* Skip group sub-statements until the next "group"
			  or "exclude" statement. */
static PROCGROUP_LIST *cur_group_list;

static PROCGROUP *
last_procgroup(void)
{
	if (skip_group)
		return NULL;
	if (!cur_group_list || PROCGROUP_LIST_EMPTY(cur_group_list)) {
		config_perror("no previous \"group\" or \"exclude\" statement");
		skip_group = 1;
		return NULL;
	}
	return PROCGROUP_LIST_LAST(cur_group_list);
}

static void
clear_last_procgroup(void)
{
	cur_group_list = NULL;
	skip_group = 0;
}

static void
parse_general_group(PROCGROUP_LIST *plist, const char *kw, char *val)
{
	PROCGROUP *pgr;

	pgr = procgroup_find(plist, val);
	if (pgr) {
		config_perror("group already defined");
		cur_group_list = NULL;
		skip_group = 1;
	} else {
		pgr = procgroup_create(val);
		if (!pgr)
			abend_nomem();
		PROCGROUP_LIST_APPEND(plist, pgr);
		cur_group_list = plist;
		skip_group = 0;
	}	
}

static void
parse_config_min(const char *kw, char *val)
{
	unsigned long n;
	PROCGROUP *grp = last_procgroup();
	if (grp && convnum(val, 0, &n) == 0) {
		grp->min_count = n;
	}
}

static void
parse_config_max(const char *kw, char *val)
{
	unsigned long n;
	PROCGROUP *grp = last_procgroup();
	if (grp && convnum(val, 0, &n) == 0) {
		grp->max_count = n;
	}
}

static int
str2field(char const *s)
{
	if (strcmp(s, "comm") == 0)
		return pf_comm;
	else if (strcmp(s, "exe") == 0)
		return pf_exe;
	else if (strcmp(s, "cmdline") == 0)
		return pf_cmdline;
	return -1;
}

static void
parse_config_field(const char *kw, char *val)
{
	PROCGROUP *grp = last_procgroup();
	if (grp) {
		int n;
		if ((n = str2field(val)) == -1)
			config_perror("unrecognized field name; "
				      "expected one of: comm, exe, cmdline");
		else
			grp->field = n;
	}
}

static void
parse_config_pattern(const char *kw, char *val)
{
	PROCGROUP *grp = last_procgroup();
	if (grp) {
		free(grp->pattern);
		if (!(grp->pattern = strdup(val)))
			abend_nomem();
		if (grp->match == match_rx)
			procgroup_compile_rx(grp);
	}
}

static int
str2match(char const *s)
{
	if (strcmp(s, "exact") == 0)
		return match_exact;
	else if (strcmp(s, "basename") == 0)
		return match_basename;
	else if (strcmp(s, "rx") == 0 || strcmp(s, "regex") == 0)
		return match_rx;
	return -1;
}

static void
parse_config_match(const char *kw, char *val)
{
	PROCGROUP *grp = last_procgroup();
	if (grp) {
		int n;
		if ((n = str2match(val)) == -1)
			config_perror("unrecognized match type; "
				      "expected one of: exact, basename, rx, or regex");
		else
			procgroup_set_match(grp, n);
	}
}

static void
parse_config_vsize(const char *kw, char *val)
{
	PROCGROUP *grp = last_procgroup();
	if (grp) {
		unsigned long n;
		if (convnum(val, 1, &n) == 0)
			grp->vsize_max = n;
	}
}

static void
parse_config_rss(const char *kw, char *val)
{
	PROCGROUP *grp = last_procgroup();
	if (grp) {
		unsigned long n;
		if (convnum(val, 1, &n) == 0)
			grp->rss_max = n;
	}
}

static void
parse_config_cpu(const char *kw, char *val)
{
	PROCGROUP *grp = last_procgroup();
	if (grp) {
		unsigned long n;
		if (convnum(val, 0, &n) == 0) {
			if (n < 100)
				grp->cpu_max = n;
			else
				config_pwarn("CPU percentage out of range");
		}
	}
}
		
static void
parse_config_group(const char *kw, char *val)
{
	parse_general_group(&process_groups, kw, val);
}


static void
parse_config_exclude(const char *kw, char *val)
{
	parse_general_group(&exclude_groups, kw, val);
}

static void
parse_config_proc(const char *kw, char *val)
{
	char *p;
	PROCGROUP *ent;
	struct tokbuf tb;

	clear_last_procgroup();
	
	tokbuf_init(&tb, val);

	p = getstr(&tb);
	if (!p)
		return;
	ent = procgroup_create(p);
	if (!(ent->pattern = strdup(ent->name)))
		abend_nomem();
	if (tokbuf_eof(&tb)) {
		/*
		 * If neither MAX nor MIN are specified, they will default
		 * to infinity and 1 respectively ("at least one").
		 */
		ent->min_count = 1;
		ent->max_count = 0;
	} else if (getnum_dfl(&tb, &ent->max_count, "max count", 0) == 0
		   && getnum_dfl(&tb, &ent->min_count, "min count", 0) == 0) {
		if (!tokbuf_eof(&tb))
			config_pwarn("garbage after maximum count ignored");
		PROCGROUP_LIST_APPEND(&process_groups, ent);
	} else
		procgroup_free(ent);
	
	tokbuf_free(&tb);
}

static void
parse_config_ps_interval(const char *kw, char *val)
{
	unsigned long n;
	struct tokbuf tb;
	
	clear_last_procgroup();

	tokbuf_init(&tb, val);

	if (getnum(&tb, &n, "interval"))
		return;
	if (n > UINT_MAX) {
		config_perror("interval out of allowed range");
		return;
	}
	ps_interval = n;
	tokbuf_free(&tb);
}

static void
parse_config_pidfile(const char *kw, char *val)
{
	pidfile = strdup(val);
}

/* defgroup
     excl:1|0
     name:STRING
     pattern:STRING
     field:0|1|2
     match:0|1|2
     min_count:NUMBER
     max_count:NUMBER
     vsize_max:NUMBER
     rss_max:NUMBER
     cpu_max:NUMBER
*/
enum {
	DT_STRING,
	DT_NUMBER,
	DT_ENUM
};

struct defgroup_field {
	char const *name;
	unsigned type;
	unsigned long maxval;
	size_t off;
	void (*setenum)(PROCGROUP *, unsigned long);
	int (*getenum)(PROCGROUP *);
};

static int
getenum_field(PROCGROUP *pgr)
{
	return pgr->field;
}

static void
setenum_field(PROCGROUP *pgr, unsigned long val)
{
	pgr->field = val;
}

static int
getenum_match(PROCGROUP *pgr)
{
	return pgr->match;
}

static void
setenum_match(PROCGROUP *pgr, unsigned long val)
{
	pgr->match = val;
}

static struct defgroup_field defgroup_fields[] = {
	{ "name",    DT_STRING, 0,
	  offsetof(struct process_group, name) },
	{ "pattern", DT_STRING, 0,
	  offsetof(struct process_group, pattern) },
	{ "field",   DT_ENUM, pf_cmdline,
	  offsetof(struct process_group, field),
	  setenum_field, getenum_field },
	{ "match",   DT_ENUM, match_rx,
	  offsetof(struct process_group, match),
	  setenum_match, getenum_match },
	{ "min_count", DT_NUMBER, 0,
	  offsetof(struct process_group, min_count) },
	{ "max_count", DT_NUMBER, 0,
	  offsetof(struct process_group, max_count) },
	{ "vsize_max", DT_NUMBER, 0,
	  offsetof(struct process_group, vsize_max) },
	{ "rss_max",   DT_NUMBER, 0,
	  offsetof(struct process_group, rss_max) },
	{ "cpu_max",   DT_ENUM, 100,
	  offsetof(struct process_group, cpu_max) },
};
static int defgroup_field_count = sizeof(defgroup_fields) / sizeof(defgroup_fields[0]);

static void
parse_config_defgroup(const char *kw, char *val)
{
	struct tokbuf tb;
	char *tok, *p;
	PROCGROUP *pgr, *pgr_old;
	PROCGROUP_LIST *plist;
	unsigned long n;
	int i;
	
	tokbuf_init(&tb, val);

	pgr = calloc(1, sizeof(pgr[0]));
	if (!pgr)
		abend_nomem();

	if (getnum(&tb, &n, "field 1"))
		goto err;
	switch (n) {
	case 0:
		plist = &process_groups;
		break;
	case 1:
		plist = &exclude_groups;
		break;
	default:
		config_perror("field 1: bad value");
		goto err;
	}

	for (i = 0; i < defgroup_field_count; i++) {
		switch (defgroup_fields[i].type) {
		case DT_STRING:
			tok = getstr(&tb);
			if (!tok) {
				fconfig_perror("failed at field #%d (%s)",
					       i + 2, defgroup_fields[i].name);
				goto err;
			}
			
			p = strdup(tok);
			if (!p)
				abend_nomem();
			*(char **)((char*)pgr + defgroup_fields[i].off) = p;
			break;

		case DT_NUMBER:
			if (getnum(&tb, &n, defgroup_fields[i].name))
				goto err;
			if (defgroup_fields[i].maxval &&
			    n > defgroup_fields[i].maxval) {
				fconfig_perror("value of field #%d (%s) is out of range",
					       i + 2, defgroup_fields[i].name);
				goto err;
			}
			*(unsigned long*)((char*)pgr + defgroup_fields[i].off) = n;
			break;

		case DT_ENUM:
			if (getnum(&tb, &n, defgroup_fields[i].name))
				goto err;
			if (defgroup_fields[i].maxval &&
			    n > defgroup_fields[i].maxval) {
				fconfig_perror("value of field #%d (%s) is out of range",
					       i + 2, defgroup_fields[i].name);
				goto err;
			}
			if (defgroup_fields[i].setenum)
				defgroup_fields[i].setenum(pgr, n);
			else
				*(int*)((char*)pgr + defgroup_fields[i].off) = n;
			break;

		default:
			abort();
		}
	}

	if (pgr->match == match_rx) {
		n = regcomp(&pgr->rx, pgr->pattern, REG_EXTENDED|REG_NOSUB);

		if (n) {
			char errbuf[512];
			regerror(n, &pgr->rx, errbuf, sizeof(errbuf));
			config_perror(errbuf);
			goto err;
		}
	}
	
	pgr->modified = 1;
	pgr_old = procgroup_find(plist, pgr->name);
	if (pgr_old)
		procgroup_replace(pgr_old, pgr);
	else
		PROCGROUP_LIST_APPEND(plist, pgr);
	return;
err:
	procgroup_free(pgr);
}

typedef struct memstorage {
	char *bufptr;
	size_t bufsize;
	size_t buflen;
	jmp_buf jbuf;
} MEMSTORAGE;

#define MEMSTORAGE_INITIALIZER { NULL, 0, 0 }

static inline void
memstorage_free(struct memstorage *store)
{
	free(store->bufptr);
}

static void
memstorage_append(struct memstorage *store, char const *str, size_t len)
{
	while (store->buflen + len > store->bufsize) {
		char *p = a2nrealloc(store->bufptr, &store->bufsize, 1);
		if (!p)
			longjmp(store->jbuf, 1);
		store->bufptr = p;
	}
	memcpy(store->bufptr + store->buflen, str, len);
	store->buflen += len;
}

static inline void
memstorage_append_char(struct memstorage *store, char c)
{
	memstorage_append(store, &c, 1);
}

static void
memstorage_append_num(struct memstorage *store, unsigned long n)
{
	size_t start = store->buflen;
	char *p, *q;
	do {
		memstorage_append_char(store, n % 10 + '0');
		n /= 10;
	} while (n > 0);

	p = store->bufptr + start;
	q = store->bufptr + store->buflen - 1;

	while (p < q) {
		char t = *p;
		*p = *q;
		*q = t;
		p++;
		q--;
	}
}

static void
memstorage_append_str(struct memstorage *store, char const *str)
{
	memstorage_append_char(store, '"');
	while (1) {
		size_t len = strcspn(str, "\\\"");
		memstorage_append(store, str, len);
		str += len;
		if (*str) {
			memstorage_append_char(store, '\\');
			memstorage_append_char(store, *str);
			str++;
		} else
			break;
	}
	memstorage_append_char(store, '"');
}		

static void
store_config_defgroup(char const *type, int list, PROCGROUP *pgr)
{
	MEMSTORAGE store = MEMSTORAGE_INITIALIZER;
	int i;

	if (setjmp(store.jbuf)) {
		snmp_log(LOG_ERR, "not enough memory");
		memstorage_free(&store);
		return;
	}
	
	memstorage_append(&store, "defgroup ", 9);
	memstorage_append_num(&store, list);

	for (i = 0; i < defgroup_field_count; i++) {
		memstorage_append_char(&store, ' ');
		switch (defgroup_fields[i].type) {
		case DT_STRING:
			memstorage_append_str(&store, 
					      *(char**)((char*)pgr + defgroup_fields[i].off));
			break;

		case DT_NUMBER:
			memstorage_append_num(&store,
					      *(unsigned long*)((char*)pgr + defgroup_fields[i].off));
			break;

		case DT_ENUM:
			memstorage_append_num(&store,
			     defgroup_fields[i].getenum
 			         ? defgroup_fields[i].getenum(pgr)
			         : *(int*)((char*)pgr + defgroup_fields[i].off));
			break;
		}
	}
	memstorage_append_char(&store, 0);
	read_config_store(type, store.bufptr);
	memstorage_free(&store);
}


struct hostproc_mib_config {
        char *token;
        char *help;
        void (*parser)(const char *token, char *line);
        void (*releaser)(void);
	void (*fini)(void);
};

static struct hostproc_mib_config config[] = {
	{ "psInterval", "NUMBER",
	  parse_config_ps_interval },
	{ "pidfile", "FILE",
	  parse_config_pidfile },
	{ "proc", "NAME [MAX] [MIN]",
	  parse_config_proc },
	{ "group", "NAME",
	  parse_config_group },
	{ "exclude", "NAME",
	  parse_config_exclude },
	{ "min", "NUM",
	  parse_config_min },
	{ "max", "NUM",
	  parse_config_max },
	{ "field", "comm|exe|cmdline",
	  parse_config_field },
	{ "pattern", "STRING",
	  parse_config_pattern },
	{ "match", "exact|basename|rx",
	  parse_config_match },
	{ "vsize", "SIZE",
	  parse_config_vsize },
	{ "rss", "SIZE",
	  parse_config_rss },
	{ "cpu", "PCT",
	  parse_config_cpu },
	{ "defgroup", "excl:1|0 name:STRING pattern:STRING field:0|1|2 match:0|1|2 min_count:NUMBER max_count:NUMBER vsize_max:NUMBER rss_max:NUMBER cpu_max:NUMBER",
	  parse_config_defgroup },
	{ NULL }
};

static void
procgroup_list_store(int ln, PROCGROUP_LIST *plist)
{
	PROCGROUP *p;
	char *type = netsnmp_ds_get_string(NETSNMP_DS_LIBRARY_ID,
					   NETSNMP_DS_LIB_APPTYPE);

	PROCGROUP_LIST_LOCK(plist);
	PROCGROUP_LIST_FOREACH(p, plist) {
		if (p->modified) {
			store_config_defgroup(type, ln, p);
		}
	}
	PROCGROUP_LIST_UNLOCK(plist);
}

static int
config_store_callback(int maj, int min, void *serverarg, void *clientarg)
{
	procgroup_list_store(0, &process_groups);
	procgroup_list_store(1, &exclude_groups);
	return 0;
}

void
hostproc_init_config(void)
{
	struct hostproc_mib_config *cp;
        for (cp = config; cp->token; cp++) {
                if (!register_app_config_handler(cp->token,
						 cp->parser,
						 cp->releaser,
						 cp->help))
                        snmp_log(LOG_ERR,"can't register %s config handler\n",
				 cp->token);
        }
	snmp_register_callback(SNMP_CALLBACK_LIBRARY,
			       SNMP_CALLBACK_STORE_DATA,
			       config_store_callback,
			       NULL);
}

void
hostproc_config_finalize(void)
{
	struct hostproc_mib_config *cp;
        for (cp = config; cp->token; cp++) {
		if (cp->fini)
			cp->fini();
	}
}

void
hostproc_mib_config_help(void)
{
        int i;

        for (i = 0; config[i].token; i++) {
                printf("%s %s\n", config[i].token, config[i].help);
        }
}

static int
json_object_get_type(struct json_value *obj, char const *name, int type,
		     struct json_value **retval)
{
	struct json_value *v;
	if (json_object_get(obj, name, &v))
		return 1;
	if (v->type != type) {
		snmp_log(LOG_ERR, "%s: bad field type\n", name);
		return -1;
	}
	*retval = v;
	return 0;
}

static int
procgroup_delete_unlocked(PROCGROUP_LIST *plist, char const *name)
{
	PROCGROUP *p = procgroup_find(plist, name);
	if (!p) {
		snmp_log(LOG_ERR, "group delete failed: no group named %s exists\n",
			 name);
		return -1;
	}
	PROCGROUP_LIST_REMOVE(plist, p);
	procgroup_free(p);
	return 0;
}

static int
config_procgroup_fill(PROCGROUP *pg, struct json_value *jreq)
{
	struct json_value *val;
	PROCGROUP_LIST *plist = &process_groups;
	PROCGROUP *p;
	int n;
	static struct limit {
		char const *name;
		size_t off;
		unsigned long maxval;
	} limits[] = {
		{ "minCount", offsetof(struct process_group, min_count) },
		{ "maxCount", offsetof(struct process_group, max_count) },
		{ "maxVSZ",   offsetof(struct process_group, vsize_max) },
		{ "maxRSS",   offsetof(struct process_group, rss_max) },
		{ "maxCPU",   offsetof(struct process_group, cpu_max), 100 },
	};
	static int numlimits = sizeof(limits) / sizeof(limits[0]);
	int i;
	
	switch (json_object_get_type(jreq, "excluded", json_bool, &val)) {
	case 0:
		if (val->v.b)
			plist = &exclude_groups;
		break;
	case 1:
		break;
	default:
		return SNMP_ERR_BADVALUE;
	}

	switch (json_object_get_type(jreq, "delete", json_bool, &val)) {
	case 0:
		if (val->v.b) {
			PROCGROUP_LIST_LOCK(plist);
			n = procgroup_delete_unlocked(plist, pg->name);
			PROCGROUP_LIST_UNLOCK(plist);
			if (n == 0) {
				reset_start_time();
				return SNMP_ERR_NOERROR;
			}
			return SNMP_ERR_NOSUCHNAME;
		}
		break;
	case 1:
		break;
	default:
		return SNMP_ERR_BADVALUE;
	}

	switch (json_object_get_type(jreq, "field", json_string, &val)) {
	case 0:
		if ((n = str2field(val->v.s)) == -1) {
			snmp_log(LOG_ERR, "%s: unrecognized field name\n",
				 val->v.s);
			return SNMP_ERR_INCONSISTENTVALUE;
		}
		pg->field = n;
		break;
	case 1:
		pg->field = pf_comm;
		break;
	default:
		return SNMP_ERR_BADVALUE;
	}

	switch (json_object_get_type(jreq, "match", json_string, &val)) {
	case 0:
		if ((n = str2match(val->v.s)) == -1) {
			snmp_log(LOG_ERR, "%s: unrecognized match type\n",
				 val->v.s);
			return SNMP_ERR_INCONSISTENTVALUE;
		}
		pg->match = n;
		break;
	case 1:
		pg->match = match_exact;
		break;
	default:
		return SNMP_ERR_BADVALUE;
	}

	switch (json_object_get_type(jreq, "pattern", json_string, &val)) {
	case 0:
		if ((pg->pattern = strdup(val->v.s)) == NULL) {
			snmp_log(LOG_ERR, "%s\n", strerror(errno));
			return SNMP_ERR_RESOURCEUNAVAILABLE;
		}
		break;

	case 1:
		if ((pg->pattern = strdup(pg->name)) == NULL) {
			snmp_log(LOG_ERR, "%s\n", strerror(errno));
			return SNMP_ERR_RESOURCEUNAVAILABLE;
		}
		break;
		
	default:
		return SNMP_ERR_BADVALUE;
	}

	if (pg->match == match_rx) {
		n = regcomp(&pg->rx, pg->pattern, REG_EXTENDED|REG_NOSUB);

		if (n) {
			char errbuf[512];
			regerror(n, &pg->rx, errbuf, sizeof(errbuf));
			snmp_log(LOG_ERR, "%s\n", errbuf);
			return SNMP_ERR_INCONSISTENTVALUE;
		}
	}

	for (i = 0; i < numlimits; i++) {
		switch (json_object_get_type(jreq, limits[i].name,
					     json_number, &val)) {
		case 0:
			if (val->v.n < 0 || val->v.n > ULONG_MAX ||
			    (limits[i].maxval > 0 &&
			     val->v.n > limits[i].maxval)) {
				snmp_log(LOG_ERR, "%s: variable value out of range\n", limits[i].name);
				return SNMP_ERR_INCONSISTENTVALUE;
			}
			*(unsigned long*)((char*)pg + limits[i].off) = val->v.n;
			break;
		case 1:
			break;
		default:
			return SNMP_ERR_BADVALUE;
		}
	}
	
	pg->modified = 1;

	PROCGROUP_LIST_LOCK(plist);
	p = procgroup_find(plist, pg->name);
	if (p) {
		procgroup_replace(p, pg);
		reset_start_time();
	} else
		PROCGROUP_LIST_APPEND(plist, pg);
	table_invalidate();
	PROCGROUP_LIST_UNLOCK(plist);	
	return SNMP_ERR_NOERROR;
}
	
int
config_request_process(struct json_value *jreq)
{
	struct json_value *val;
	PROCGROUP *pg;
	int ret;
	
	switch (json_object_get_type(jreq, "name", json_string, &val)) {
	case 0:
		break;
		
	case 1:
		snmp_log(LOG_ERR, "%s: mandatory field not present\n",
			 "name");
		return SNMP_ERR_INCONSISTENTVALUE;

	default:
		return SNMP_ERR_BADVALUE;
	}

	pg = calloc(1, sizeof(*pg));
	if (!pg) {
		snmp_log(LOG_ERR, "%s\n", strerror(errno));
		return SNMP_ERR_RESOURCEUNAVAILABLE;
	}
	if ((pg->name = strdup(val->v.s)) == NULL) {
		snmp_log(LOG_ERR, "%s\n", strerror(errno));
		ret = SNMP_ERR_RESOURCEUNAVAILABLE;
	} else
		ret = config_procgroup_fill(pg, jreq);

	if (ret != SNMP_ERR_NOERROR)
		procgroup_free(pg);
	
	return ret;
}

