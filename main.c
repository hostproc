/* hostproc - SNMP agent for monitoring detailed list of running processes 
   Copyright (C) 2020 Sergey Poznyakoff

   Hostproc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Hostproc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with hostproc. If not, see <http://www.gnu.org/licenses/>. */
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>
#include <assert.h>
#include "hostproc.h"
#include "hostproc_mib.h"
#include "json.h"

char const *progname;
unsigned long Hertz;
unsigned long page_size; /* Page size in kilobytes */
unsigned ps_interval = 30;
char *pidfile;
struct timespec start_time;
PROCGROUP_LIST process_groups = PROCGROUP_LIST_INITIALIZER(process_groups);
PROCGROUP_LIST exclude_groups = PROCGROUP_LIST_INITIALIZER(exclude_groups);

static void
set_progname(char *arg0)
{
	char *p = strrchr(arg0, '/');
	progname = p ? p + 1 : arg0;
}

void
abend(char const *fmt, ...)
{
	va_list ap;
	
	va_start(ap, fmt);
	snmp_vlog(LOG_CRIT, fmt, ap);
	snmp_log(LOG_CRIT, "\n");
	va_end(ap);
	exit(EX_FAILURE);
}

void *
a2nrealloc(void *p, size_t *pn, size_t s)
{
	size_t n = *pn;
	char *newp;
	
	if (!p) {
		if (!n) {
			/* The approximate size to use for initial small
			   allocation requests, when the invoking code
			   specifies an old size of zero.  64 bytes is
			   the largest "small" request for the
			   GNU C library malloc.  */
			enum { DEFAULT_MXFAST = 64 };

			n = DEFAULT_MXFAST / s;
			n += !n;
		}
	} else {
		/* Set N = ceil (1.5 * N) so that progress is made if N == 1.
		   Check for overflow, so that N * S stays in size_t range.
		   The check is slightly conservative, but an exact check isn't
		   worth the trouble.  */
		if ((size_t) -1 / 3 * 2 / s <= n) {
			errno = ENOMEM;
			return NULL;
		}
		n += (n + 1) / 2;
	}

	newp = realloc(p, n * s);
	if (!newp)
		return NULL;
	*pn = n;
	return newp;
}


static void
set_hertz(void)
{
	long hz;
	
	if ((hz = sysconf(_SC_CLK_TCK)) > 0)
		Hertz = hz;
	else
		abend("can't determine kernel clock tick rate: %s",
		      strerror(errno));
}

static void
set_page_size(void)
{
#ifdef _SC_PAGESIZE
	page_size = sysconf(_SC_PAGESIZE) / 1024;
#endif
	if (page_size == 0) {
		FILE *fp;
		unsigned long vmrss = 0;
		struct process_stat st;
		int rc;
		int nl;
#define MY_STAT_FILE PROC_DIR "/self/" PID_STAT_FILE
#define MY_STATUS_FILE PROC_DIR "/self/" PID_STATUS_FILE		
		if ((fp = fopen(MY_STAT_FILE, "r")) == NULL) {
			abend("can't open %s: %s", MY_STAT_FILE,
			      strerror(errno));
		}
		rc = proc_stat_read(fp, &st);
		fclose(fp);
		if (rc) {
			abend("%s: %s",
			      MY_STAT_FILE,
			      (rc == 1) ? "bad format"
			                : "out of memory reading file");
		}			
		if (st.rss == 0)
			abend("can't determine page size");

		if ((fp = fopen(MY_STATUS_FILE, "r")) == NULL) {
			abend("can't open %s: %s", MY_STATUS_FILE,
			      strerror(errno));
		}
		nl = 1;
		while (!feof(fp) && !ferror(fp)) {
			if (nl) {
				if (fscanf(fp, "VmRSS: %lu", &vmrss) == 1)
					break;
			}
			nl = fgetc(fp) == '\n';
		}
		fclose(fp);
		if (vmrss == 0)
			abend("can't determine page size");
		page_size = vmrss / st.rss;
		free(st.comm);
	}
}

void *
hostproc_thr_agent(void *arg)
{
        while (1) {
                agent_check_and_process(1);
        }
}

static void
signull(int sig)
{
}

static void
usage(FILE *fp)
{
	fprintf(stderr, "usage: %s [-ACd] [-D TOKENS] [-L LOGOPT] [-c FILE]\n",
		progname);
	fprintf(stderr, "Enhanced process monitoring agent\n");
	fprintf(stderr, "\nOPTIONS:\n\n");
	fprintf(fp,
		"  -A          append to the log file rather than truncating it\n");
	fprintf(fp,
		"  -C          do not read any configuration files except the ones optionally\n"
		"              specified by the -c option\n");
	fprintf(fp,
		"  -D TOKEN[,TOKEN...]\n"
		"              turn on debugging output for the given TOKENs\n");
	fprintf(fp,
		"  -H          display a list of configuration file directives understood\n"
		"              by the agent and then exit\n");
	fprintf(fp,
		"  -L[efos]    configure logging\n");
	fprintf(fp,
		"  -c FILE     read FILE as a configuration file (or a comma-separated list of\n"
		"              configuration files)\n");
	fprintf(fp,
		"  -d          dump (in hexadecimal) the sent and received SNMP packets\n");
	fprintf(fp,
		"  -f          remain in foreground\n");
	fprintf(fp,
		"  -p FILE     write PID to FILE\n");
	fprintf(fp,
		"  -h, -?      print this help summary\n");
	fprintf(stderr, "\n");
	fprintf(fp, "default configuration path is: %s\n",
		get_configuration_directory());
}

void
pidfile_remove(void)
{
	if (pidfile && unlink(pidfile))
		snmp_log(LOG_ERR, "cannot unlink pidfile `%s': %s\n",
			 pidfile,
			 strerror(errno));
}

void
pidfile_create(void)
{
	FILE *fp;

	if (!pidfile)
		return;

	fp = fopen(pidfile, "w");
	if (!fp) {
		snmp_log(LOG_ERR, "cannot create pidfile `%s': %s\n", pidfile,
			 strerror(errno));
		exit(EX_FAILURE);
	}
	fprintf(fp, "%lu", (unsigned long) getpid());
	fclose(fp);
}

/* Check whether pidfile exists and if so, whether its PID is still
   active. Exit if so. */
void
pidfile_check(void)
{
	unsigned long pid;
	FILE *fp;

	if (!pidfile)
		return;

	fp = fopen(pidfile, "r");

	if (fp) {
		if (fscanf(fp, "%lu", &pid) != 1) {
			snmp_log(LOG_ERR, "cannot get pid from pidfile `%s'\n",
				 pidfile);
		} else {
			if (kill(pid, 0) == 0) {
				snmp_log(LOG_INFO,
				    "%s appears to run with pid %lu. "
				     "If it does not, remove `%s' and retry.\n",
					 progname, pid, pidfile);
				exit(EX_FAILURE);
			}
		}

		fclose(fp);
		if (unlink(pidfile)) {
			snmp_log(LOG_ERR, "cannot unlink pidfile `%s': %s\n",
				 pidfile, strerror(errno));
			exit(EX_FAILURE);
		}
	} else if (errno != ENOENT) {
		snmp_log(LOG_ERR, "cannot open pidfile `%s': %s",
			 pidfile, strerror(errno));
		exit(EX_FAILURE);
	}
}

static char *
make_file_name(char *dir, char *file)
{
	size_t dlen = strlen(dir);
	size_t flen = strlen(file);
	size_t len;
	char *name;
		
	while (dlen > 0 && dir[dlen-1] == '/')
		dlen--;
	if (dlen == 0) {
		name = strdup(file);
		if (!name)
			abend_nomem();
	} else {
		len = dlen + flen + 2;
		name = malloc(len);
		if (!name)
			abend_nomem();
		memcpy(name, dir, dlen);
		name[dlen++] = '/';
		memcpy(name + dlen, file, flen + 1);
	}
	return name;
}

void
reset_start_time(void)
{
	clock_gettime(CLOCK_MONOTONIC, &start_time);
}

int
main(int argc, char **argv)
{
	int c;
	pthread_t tid;
	int i;
	struct sigaction act;
	sigset_t sigs;
	static int fatal_signals[] = {
		SIGHUP,
		SIGINT,
		SIGQUIT,
		SIGTERM,
		0
	};
	void *retval;
	int log_set = 0;
	int foreground = 0;
	char *p_opt = NULL;
	
	set_progname(argv[0]);
	set_hertz();
	set_page_size();
	
	while ((c = getopt(argc, argv, "ACD:HhL:c:dfp:")) != EOF) {
		switch (c) {
		case 'A':
			netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID,
					       NETSNMP_DS_LIB_APPEND_LOGFILES,
					       1);
			break;

		case 'C':
			netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID,
					       NETSNMP_DS_LIB_DONT_READ_CONFIGS,
					       1);
			break;
			
		case 'D':
#ifdef NETSNMP_NO_DEBUGGING
			fprintf(stderr, "%s: debugging not configured\n",
				progname);
			exit(EX_USAGE);
#else
			debug_register_tokens(optarg);
			snmp_set_do_debugging(1);
#endif
			break;
	
		case 'H':
			hostproc_mib_config_help();
			exit(EX_OK);
			
		case 'L':
			if (snmp_log_options(optarg, argc, argv) < 0) {
				usage(stderr);
			}
			log_set = 1;
			break;

		case 'c':
			netsnmp_ds_set_string(NETSNMP_DS_LIBRARY_ID,
					      NETSNMP_DS_LIB_OPTIONALCONFIG,
					      optarg);
			break;

		case 'd':
			netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID,
					       NETSNMP_DS_LIB_DUMP_PACKET,
					       1);
			break;

		case 'f':
			foreground = 1;
			break;
			
		case 'h':
			usage(stdout);
			exit(EX_OK);

		case 'p':
			p_opt = optarg;
			break;
			
		default:
			if (optopt == '?') {
				usage(stdout);
				exit(EX_OK);
			}
			fprintf(stderr, "%s: unrecognized option: %c\n",
				progname, optopt);
			usage(stderr);
			exit(EX_USAGE);
		}
	}

	if (!log_set)
                snmp_enable_stderrlog();
	
	netsnmp_ds_set_string(NETSNMP_DS_LIBRARY_ID,
                              NETSNMP_DS_LIB_APPTYPE, progname);

        netsnmp_ds_set_boolean(NETSNMP_DS_APPLICATION_ID,
                               NETSNMP_DS_AGENT_ROLE, 1);

	if (getuid() && !getenv("SNMP_PERSISTENT_DIR")) {
		/* Set a reasonable persistent directory */
		char *user_snmp_dir = make_file_name(getenv("HOME"), ".snmp");
		char *persist_dir = make_file_name(user_snmp_dir, "persist");
		set_persistent_directory(persist_dir);
		free(persist_dir);
		free(user_snmp_dir);
	}

	reset_start_time();
	
	SOCK_STARTUP;
	init_agent(progname);
	hostproc_init_config();
	hostproc_mib_init();
	init_snmp(progname);
	hostproc_config_finalize();

	if (p_opt)
		pidfile = p_opt;
	
	pidfile_check();
	
	if (!foreground) {
		snmp_enable_syslog_ident(progname, LOG_DAEMON);
		snmp_disable_stderrlog();
		if (netsnmp_daemonize(1, 0))
			exit(EX_OK);
	}
	pidfile_create();
	
	/* Set up signal handlers */
	sigemptyset(&sigs);

	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	act.sa_handler = signull;

	for (i = 0; fatal_signals[i]; i++) {
		sigaddset(&sigs, fatal_signals[i]);
		sigaction(fatal_signals[i], &act, NULL);
	}
	sigaddset(&sigs, SIGPIPE);
	sigaddset(&sigs, SIGALRM);
	sigaddset(&sigs, SIGCHLD);
	pthread_sigmask(SIG_BLOCK, &sigs, NULL);

	pthread_create(&tid, NULL, hostproc_thr_ps, NULL);
	pthread_create(&tid, NULL, hostproc_thr_agent, NULL);

	/* Unblock only the fatal signals */
	sigemptyset(&sigs);
	for (i = 0; fatal_signals[i]; i++) {
		sigaddset(&sigs, fatal_signals[i]);
	}
	pthread_sigmask(SIG_UNBLOCK, &sigs, NULL);

	/* Wait for signal to arrive */
	sigwait(&sigs, &i);
	
	/* Shutdown */
	pthread_cancel(tid);
	pthread_join(tid, &retval);

	pidfile_remove();
	SOCK_CLEANUP;
	snmp_shutdown(progname);
	return 0;
}

time_t
agent_hostprocUpTime(void)
{
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return ts.tv_sec - start_time.tv_sec;
}

int
hostproc_group_mgmt_set(netsnmp_handler_registration *reginfo,
			netsnmp_agent_request_info   *reqinfo,
			netsnmp_request_info         *requests)
{
	char *input;
	char *endp;
	struct json_value *jreq;
	int rc;
	int ret = SNMP_ERR_NOERROR;

	input = malloc(requests->requestvb->val_len + 1);
	if (!input) {
		snmp_log(LOG_ERR, "out of memory trying to handle set request\n");
		return SNMP_ERR_GENERR;
	}
	memcpy(input, requests->requestvb->val.string,
	       requests->requestvb->val_len);
	input[requests->requestvb->val_len] = 0;
	
	snmp_log(LOG_INFO, "set request: %s\n", input);
	rc = json_parse_string(input, &jreq, &endp);
		
	if (rc != JSON_E_NOERR) {
		snmp_log(LOG_ERR, "can't parse input string: %s\n",
			 json_strerror(rc));
		snmp_log(LOG_ERR, "input string: %s\n", input);
		snmp_log(LOG_ERR, "error in position %ld, near \"%s\"\n",
			 (long)(endp - input), endp);
		free(input);
		return rc == JSON_E_NOMEM ? SNMP_ERR_TOOBIG : SNMP_ERR_BADVALUE;
	}
	
	switch (jreq->type) {
	case json_object:
		if ((ret = config_request_process(jreq)) != SNMP_ERR_NOERROR) {
			snmp_log(LOG_ERR, "failed request was: %s\n", input);
		}
		break;

	default:
		snmp_log(LOG_ERR, "bad object type\n");
		snmp_log(LOG_ERR, "failed request was: %s\n", input);
		ret = SNMP_ERR_BADVALUE;
	}

	json_value_free(jreq);
	free(input);
	return ret;
}





