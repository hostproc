/* hostproc - SNMP agent for monitoring detailed list of running processes 
   Copyright (C) 2020 Sergey Poznyakoff

   Hostproc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Hostproc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with hostproc. If not, see <http://www.gnu.org/licenses/>. */
#include <stdio.h>
#include <regex.h>
#include <sys/queue.h>
#include <pthread.h>

enum {
	EX_OK,
	EX_FAILURE,
	EX_USAGE
};

#define PROC_DIR "/proc"
#define UPTIME_FILE  "/proc/uptime"
#define MEMINFO_FILE "/proc/meminfo" 
#define PID_STAT_FILE "stat"
#define PID_STATM_FILE "statm"
#define PID_STATUS_FILE "status"
#define PID_CMDLINE_FILE "cmdline"
#define PID_EXE_FILE "exe"

#define STAT_SCAN_FMT " %c %d %d %d %d %d %u %lu %lu %lu %lu %lu %lu %ld %ld %ld %ld %ld %ld %llu %lu %ld %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %d %d %u %u %llu %lu %ld %lu %lu %lu %lu %lu %lu %lu %d"

#define STAT_SCAN_ARGS(s) \
	&(s)->state,\
        &(s)->ppid,\
	&(s)->pgrp,\
	&(s)->session,\
	&(s)->tty_nr,\
	&(s)->tpgid,\
	&(s)->flags,\
	&(s)->minflt,\
	&(s)->cminflt,\
	&(s)->majflt,\
	&(s)->cmajflt,\
	&(s)->utime,\
	&(s)->stime,\
	&(s)->cutime,\
	&(s)->cstime,\
	&(s)->priority,\
	&(s)->nice,\
	&(s)->num_threads,\
	&(s)->itrealvalue,\
	&(s)->starttime,\
	&(s)->vsize,\
	&(s)->rss,\
	&(s)->rsslim,\
	&(s)->startcode,\
	&(s)->endcode,\
	&(s)->startstack,\
	&(s)->kstkesp,\
	&(s)->kstkeip,\
	&(s)->signal,\
	&(s)->blocked,\
	&(s)->sigignore,\
	&(s)->sigcatch,\
	&(s)->wchan,\
	&(s)->nswap,\
	&(s)->cnswap,\
	&(s)->exit_signal,\
	&(s)->processor,\
	&(s)->rt_priority,\
	&(s)->policy,\
	&(s)->delayacct_blkio_ticks,\
	&(s)->guest_time,\
	&(s)->cguest_time,\
	&(s)->start_data,\
	&(s)->end_data,\
	&(s)->start_brk,\
	&(s)->arg_start,\
	&(s)->arg_end,\
	&(s)->env_start,\
	&(s)->env_end,\
	&(s)->exit_code

struct process_stat {
	int pid;
	char *comm;
	char state;
	int ppid;
	int pgrp;
	int session;
	int tty_nr;
	int tpgid;
	unsigned flags;
	unsigned long minflt;
	unsigned long cminflt;
	unsigned long majflt;
	unsigned long cmajflt;
	unsigned long utime;
	unsigned long stime;
	long cutime;
	long cstime;
	long priority;
	long nice;
	long num_threads;
	long itrealvalue;
	unsigned long long starttime;
	unsigned long vsize;
	long rss;
	unsigned long rsslim;
	unsigned long startcode;
	unsigned long endcode;
	unsigned long startstack;
	unsigned long kstkesp;
	unsigned long kstkeip;
	unsigned long signal;
	unsigned long blocked;
	unsigned long sigignore;
	unsigned long sigcatch;
	unsigned long wchan;
	unsigned long nswap;
	unsigned long cnswap;
	int exit_signal;
	int processor;
	unsigned rt_priority;
	unsigned policy;
	unsigned long long delayacct_blkio_ticks;
	unsigned long guest_time;
	long cguest_time;
	unsigned long start_data;
	unsigned long end_data;
	unsigned long start_brk;
	unsigned long arg_start;
	unsigned long arg_end;
	unsigned long env_start;
	unsigned long env_end;
	int exit_code;
};
	
#define STAT_SCAN_COUNT 50

typedef struct process {
	int      pid;
	int      ppid;
	unsigned long uid;
	unsigned long gid;
	unsigned cpu_avg;       /* Average (life-long) CPU usage */
	unsigned cpu_inst;      /* Instantaneous CPU usage */
	unsigned long time;
	unsigned long runtime;
	int      state;
	int      nice;
	int      priority;
	unsigned long vsize;
	unsigned long rss;
	char *comm;
	char *cmdline;
	char *exe;

	int mask;               /* 1 if this process is masked off */
	
	struct process *parent;
	TAILQ_ENTRY(process) link;
	struct process *next, *prev;
} PROCESS;

typedef struct {
	size_t count;
	TAILQ_HEAD(process_list, process) list;
} PROCLIST;

#define PROCLIST_INITIALIZER(h) { 0, TAILQ_HEAD_INITIALIZER(h.list) }
#define PROCLIST_MOVE(dst,src) do {				\
		TAILQ_INIT(&dst.list);				\
		TAILQ_CONCAT(&dst.list, &src.list, link);	\
		dst.count = src.count;				\
		src.count = 0;					\
	} while(0)

enum process_match_field {
	pf_comm,
	pf_exe,
	pf_cmdline
};

enum pattern_match_type {
	match_exact,
	match_basename,
	match_rx
};

typedef struct process_group {
	char *name;                     /* Group name */
	char *pattern;                  /* Process name pattern */
	enum process_match_field field; /* Field (of PROCESS) to match against
					   the pattern. */
	enum pattern_match_type match;  /* Match type */
	regex_t rx;                     /* Compiled regex, if match_rx. */
	/* Statistics summary */
	unsigned long count;
	unsigned long cpu_inst_max;     /* Maximum CPU usage. */
	double cpu_avg_sum;             /* Sum of average CPU usage. */
	double cpu_inst_sum;            /* Sum of instantaneous CPU usage. */
	unsigned long vsize;
	unsigned long rss;
	/* Limits */
	unsigned long min_count;
	unsigned long max_count;
	unsigned long vsize_max;
	unsigned long rss_max;
	unsigned long cpu_max;

	int modified;
	TAILQ_ENTRY(process_group) link;
} PROCGROUP;

#define PROCGROUP_NEXT(p) TAILQ_NEXT(p, link)

TAILQ_HEAD(process_group_list, process_group);

typedef struct procgroup_list {
	struct process_group_list list;
	pthread_mutex_t mutex;
} PROCGROUP_LIST;

#define PROCGROUP_LIST_INITIALIZER(h) \
	{ TAILQ_HEAD_INITIALIZER((h).list), PTHREAD_MUTEX_INITIALIZER }
#define PROCGROUP_LIST_LOCK(h) pthread_mutex_lock(&(h)->mutex)
#define PROCGROUP_LIST_UNLOCK(h) pthread_mutex_unlock(&(h)->mutex)
#define PROCGROUP_LIST_EMPTY(h) TAILQ_EMPTY(&(h)->list)
#define PROCGROUP_LIST_FOREACH(p, h) TAILQ_FOREACH(p, &(h)->list, link)
#define PROCGROUP_LIST_APPEND(h, p) TAILQ_INSERT_TAIL(&(h)->list, p, link)
#define PROCGROUP_LIST_FIRST(h) TAILQ_FIRST(&(h)->list)
#define PROCGROUP_LIST_LAST(h) TAILQ_LAST(&(h)->list, process_group_list)
#define PROCGROUP_LIST_REMOVE(h,p) TAILQ_REMOVE(&(h)->list, p, link)

enum {
	MEMINFO_MEM_TOTAL,
	MEMINFO_MEM_AVAILABLE,
	MEMINFO_MEM_FREE,
	MEMINFO_SWAP_TOTAL,
	MEMINFO_SWAP_FREE,
	MEMINFO_NFIELDS
};

unsigned long meminfo_get(int idx);

extern unsigned long Hertz;
extern unsigned long page_size;
extern unsigned ps_interval;
extern char *pidfile;
extern PROCGROUP_LIST exclude_groups;
extern PROCGROUP_LIST process_groups;

int proc_stat_read(FILE *fp, struct process_stat *st);
	
int proctab_foreach(int (*handler)(PROCESS *, void *), void *data);
void *hostproc_thr_ps(void *arg);
time_t agent_hostprocUpTime(void);
void reset_start_time(void);

void hostproc_init_config(void);
void hostproc_config_finalize(void);
void hostproc_mib_config_help(void);

static inline void
procgroup_stat_init(PROCGROUP *pgroup)
{
	pgroup->count = 0;
	pgroup->cpu_inst_max = 0;
	pgroup->cpu_avg_sum = 0;
	pgroup->cpu_inst_sum = 0;
	pgroup->vsize = 0;
	pgroup->rss = 0;
}

static inline void
procgroup_stat_update(PROCGROUP *pgroup, PROCESS *pp)
{
	pgroup->count++;
	if (pp->cpu_inst > pgroup->cpu_inst_max)
		pgroup->cpu_inst_max = pp->cpu_inst;
	pgroup->cpu_avg_sum += pp->cpu_avg;
	pgroup->cpu_inst_sum += pp->cpu_inst;
	pgroup->vsize += pp->vsize;
	pgroup->rss += pp->rss;
}

enum {
	SNMPv2_TRUE = 1,
	SNMPv2_FALSE = 2
};

static inline unsigned long
procgroup_ok_status(PROCGROUP *pgroup)
{
	if ((pgroup->min_count > 0 && pgroup->count < pgroup->min_count)
	    || (pgroup->max_count > 0 && pgroup->count > pgroup->max_count)
	    || (pgroup->vsize_max > 0 && pgroup->vsize > pgroup->vsize_max)
	    || (pgroup->rss_max > 0 && pgroup->rss > pgroup->rss_max)
	    || (pgroup->cpu_max > 0 && pgroup->cpu_inst_max > pgroup->cpu_max))
		return SNMPv2_FALSE;
	return SNMPv2_TRUE;
}

PROCGROUP *procgroup_create(char const *str);
void procgroup_free(PROCGROUP *ent);

int procgroup_match(PROCESS *proc, PROCGROUP *pat);
PROCGROUP *procgroup_list_match_first(PROCESS *proc,
					  PROCGROUP_LIST *pat);
PROCGROUP *procgroup_match_next(PROCESS *proc, PROCGROUP *ent);


void abend(char const *fmt, ...);
static inline void abend_nomem(void) { abend("out of memory"); }

void *a2nrealloc(void *p, size_t *pn, size_t s);
int proctab_scan(void);

void proctab_destroy(void);
void proctab_free(void);

int proctab_sieve(PROCGROUP_LIST *);
void proctab_print(void);

void table_invalidate(void);

struct json_value;
int config_request_process(struct json_value *json);

