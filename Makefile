# Don't edit this file, unless you really need to.
# Instead, run
#    make config
# and edit the file config.mk to your liking.

# hostproc - SNMP agent for monitoring detailed list of running processes 
# Copyright (C) 2020 Sergey Poznyakoff
#
# Hostproc is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3 of the License, or (at your
# option) any later version.
#
# Hostproc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with hostproc. If not, see <http://www.gnu.org/licenses/>. */

PACKAGE = hostproc
VERSION = 0.3

ifneq (,$(wildcard config.mk))
  include config.mk
endif

PREFIX ?= /usr/local
SBINDIR ?= $(PREFIX)/sbin
DATAROOTDIR ?= $(PREFIX)/share
MANDIR ?= $(DATAROOTDIR)/man
MAN8DIR := $(MANDIR)/man8
INFODIR ?= $(DATAROOTDIR)/info

CFLAGS = -ggdb -Wall
CPPFLAGS =

ETAGS     = etags
ETAGSFLAGS=

# #######################
# Net-SNMP configuration
# #######################

NET_SNMP_CONFIG  ?= net-snmp-config

ifeq ($(shell $(NET_SNMP_CONFIG) --version),)
  $(error Net-SNMP does not seem to be installed ($(NET_SNMP_CONFIG) --version failed))
endif

NET_SNMP_LIBS    ?= $(shell $(NET_SNMP_CONFIG) --agent-libs)
NET_SNMP_EXLIBS  ?= $(shell $(NET_SNMP_CONFIG) --external-libs)
NET_SNMP_MIBDIRS ?= $(shell $(NET_SNMP_CONFIG) --mibdirs)
NET_SNMP_MIBS    ?= $(shell $(NET_SNMP_CONFIG) --default-mibs)

MIBDIR ?= $(DATAROOTDIR)/snmp/mibs
#MIBDIR ?= $(lastword $(subst :, , $(NET_SNMP_MIBDIRS)))

# #######################
# Built targets
# #######################

PROGRAM = hostproc

SOURCES = \
 main.c\
 proctab.c\
 table.c\
 config.c\
 hostproc_mib.c\
 json.c

HEADERS = \
 hostproc.h\
 hostproc_mib.h\
 json.h

BUILT_SOURCES = hostproc_mib.c hostproc_mib.h
MANPAGES = doc/hostproc.8
MIBS = HOSTPROC-MIB.txt
EXTRA_DIST = COPYING NEWS README Makefile hostproc_mib.mib2c test/snmpd.conf

OBJECTS = $(SOURCES:.c=.o)

LIBS = \
  -lpthread \
 $(NET_SNMP_LIBS)\
 $(NET_SNMP_EXLIBS)

INFOFILES = doc/hostproc.info
TEXINFOFILES = doc/hostproc.texi doc/version.texi doc/HOSTPROC-MIB.texi doc/fdl.texi doc/texinfo.tex
DOCTMP = doc/hostproc.t2d doc/hostproc.html doc/hostproc.dvi doc/hostproc.ps doc/hostproc.pdf 

all: hostproc $(INFOFILES)

hostproc: $(OBJECTS)
	$(CC) $(CFLAGS) -ohostproc $(OBJECTS) $(LIBS)

$(OBJECTS): $(HEADERS)

main.o table.o: hostproc_mib.h
hostproc_mib.h: hostproc_mib.c
hostproc_mib.c: hostproc_mib.mib2c HOSTPROC-MIB.txt

.SUFFIXES: .mib2c .c .o

.mib2c.c:
	MIBDIRS=.:${NET_SNMP_MIBDIRS} \
         MIBS="+HOSTPROC-MIB" \
          mib2c -c $< -f $@ hostprocMIB

# #######################
# Maintenance targets
# #######################

clean:
	rm -rf hostproc $(OBJECTS) $(DOCTMP)

allclean: clean
	rm -f config.mk

devclean: allclean
	rm -f $(BUILT_SOURCES) $(INFOFILES)

tags:
	$(ETAGS) $(ETAGSFLAGS) $(SOURCES) $(HEADERS)

# #######################
# Distribution rules
# #######################

DISTDIR   = $(PACKAGE)-$(VERSION)
DISTFILES = $(SOURCES) $(HEADERS) $(MIBS) $(MANPAGES) $(INFOFILES) $(TEXINFOFILES) $(EXTRA_DIST)

distdir:
	test -d $(DISTDIR) || mkdir $(DISTDIR)
	tar -c -h -f - $(DISTFILES) | tar -C $(DISTDIR) -x -f -

dist: distdir
	tar zcf $(DISTDIR).tar.gz $(DISTDIR)
	rm -rf $(DISTDIR)

distcheck: dist
	tar xfz $(DISTDIR).tar.gz
	if $(MAKE) -C $(DISTDIR) $(DISTCHECKFLAGS); then \
	  echo "$(DISTDIR).tar.gz ready for distribution"; \
	  rm -rf $(DISTDIR); \
	else \
	  exit 2; \
	fi

# #######################
# Installation
# #######################

DESTDIR =

SBINFILES := $(PROGRAM)

install-sbin: $(SBINFILES)
	install -d $(DESTDIR)$(SBINDIR)
	install $(SBINFILES) $(DESTDIR)$(SBINDIR)

install-mibs: $(MIBS)
	install -d $(DESTDIR)$(MIBDIR)
	install -m 644 $(MIBS) $(DESTDIR)$(MIBDIR)

install-man: install-man8

install-man8: $(MANPAGES)
	install -d $(DESTDIR)$(MAN8DIR)
	for file in $(MANPAGES); do echo $$file; done |\
	  grep '\.8[a-z]*$$'|\
        while read file; \
	do \
	  install -m 644 $$file $(DESTDIR)$(MAN8DIR);\
	done

install-info: $(INFOFILES)
	install -d $(DESTDIR)$(INFODIR)
	for file in $(INFOFILES); do \
		install -m 644 $$file $(DESTDIR)$(INFODIR);\
		install-info --info-dir="$(DESTDIR)$(INFODIR)" "$(DESTDIR)$(INFODIR)/$$(basename $$file)"; \
	done

install: install-sbin install-mibs install-man install-info

# #######################
# Documentation
# #######################

.SUFFIXES: .dvi .html .info .pdf .ps .txt .texi

docdir=doc
TEXINPUTS=$(docdir)
doc/hostproc.info: doc/hostproc.texi doc/version.texi doc/HOSTPROC-MIB.texi
	$(MAKEINFO) $(MAKEINFOFLAGS) -I $(docdir) -odoc/ doc/hostproc.texi
doc/version.texi: Makefile doc/hostproc.texi
	{ \
		echo "@set UPDATED $$(date +'%d %B %Y')"; \
	        echo "@set EDITION $(VERSION)"; \
		echo "@set VERSION $(VERSION)"; \
	} > doc/version.texi

doc/HOSTPROC-MIB.texi: HOSTPROC-MIB.txt
	sed -r \
             -e 's/([{}@])/@\1/g' \
             -e '/^([^[:space:]]+)[[:space:]]+OBJECT-TYPE$$/{' \
             -eh \
             -e 's//@kwindex \1/' \
             -eG \
             -e'}' \
        HOSTPROC-MIB.txt > doc/HOSTPROC-MIB.texi

doc/hostproc.dvi: doc/hostproc.texi
doc/hostproc.pdf: doc/hostproc.texi
doc/hostproc.html: doc/hostproc.texi

TEXI2DVI = texi2dvi
TEXI2PDF = $(TEXI2DVI) --pdf --batch
MAKEINFOHTML = $(MAKEINFO) --html

.texi.dvi:
	MAKEINFO='$(MAKEINFO) $(MAKEINFOFLAGS) -I $(docdir)' \
	$(TEXI2DVI) --build-dir=$(@:.dvi=.t2d) -o $@ $<

.texi.pdf:
	MAKEINFO='$(MAKEINFO) $(MAKEINFOFLAGS) -I $(docdir)' \
	$(TEXI2PDF) --build-dir=$(@:.pdf=.t2d) -o $@ $<

.texi.html:
	rm -rf $(@:.html=.htp)
	if $(MAKEINFOHTML) $(MAKEINFOFLAGS) -I $(docdir) \
             -o $(@:.html=.htp) $<; \
        then \
          rm -rf $@ && mv $(@:.html=.htp) $@; \
        else \
          rm -rf $(@:.html=.htp); exit 1; \
        fi

webdoc:
	$(MAKEINFO) -I $(docdir) --html \
                    -c EXTRA_HEAD='<link rel="stylesheet" type="text/css" href="/css/info.css"/><link rel="stylesheet" type="text/css" href="/css/project.css"/><script src="/js/modernizr.js" type="text/javascript"></script><script src="/js/info.js" type="text/javascript"></script>' doc/hostproc.texi -o $(docdir)/webdoc


# #######################
# Configuration
# #######################

.PHONY: config
config:
	@{ \
		echo "# Compilation flags for cc"; \
		echo "CFLAGS = $(CFLAGS)"; \
		echo "# Include paths (-I DIR), and other preprocessor flags";\
		echo "CPPFLAGS = $(CPPFLAGS)"; \
		echo "# Name of the net-snmp-config program"; \
		echo "NET_SNMP_CONFIG = $(NET_SNMP_CONFIG)"; \
		echo "# Installation prefix"; \
		echo "PREFIX = $(PREFIX)"; \
		echo ""; \
		echo "#"; \
		echo "# The following variables are normally deduced from PREFIX"; \
		echo "# You may override them, if you need."; \
		echo "#"; \
		echo ""; \
		echo "# Installation directory for the hostproc binary"; \
		echo $(if $(filter $(origin SBINDIR),file), \
                     '# SBINDIR = $$(PREFIX)/sbin', \
		     "SBINDIR = $(SBINDIR)"); \
		echo "# Installation directory for manual pages"; \
		echo $(if $(filter $(origin MANDIR),file), \
                     '# MANDIR = $$(PREFIX)/man', \
		     "MANDIR = $(MANDIR)"); \
		echo "# Installation directory for HOSTPROC-MIB.txt"; \
		echo $(if $(filter $(origin MIBDIR),file), \
                     '# MIBDIR = $$(PREFIX)/share/snmp/mibs', \
		     "MIBDIR = $(MIBDIR)"); \
	} > config.mk; \
	echo "Configuration stored in config.mk";
