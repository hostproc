#!/bin/sh
# Read the description of /proc/[PID]/status fields from the proc(5)
# manpage, parse it and produce to the standard output the scanf format 
# string, list of variable declarations and a corresponding list of 
# arguments for scanf.

data=/tmp/$$
trap 'rm $data' INT STOP QUIT

man 5 proc |\
  col -b |\
  sed -r\
      -n\
      -e '1,/^[[:space:]]+\/proc\/\[pid\]\/stat[[:space:]]*$/d'\
      -e '/^[[:space:]]+\/proc\//,$d'\
      -e 's/^[[:space:]]+\([[:digit:]]+\)[[:space:]]+([^[:space:]]+)[[:space:]]+(%[^[:space:]]+).*/\1 \2/p' > $data

awk 'BEGIN {
          delim="#define STAT_SCAN_FMT \""
          print ""
     }
     { printf "%s%s", delim, $2 == "%s" ? "%ms" : $2; delim = " "}
      END { print "\"" }' $data

awk 'BEGIN {
          print ""
          printf "#define STAT_SCAN_ARGS "
	  delim = "&"
     }
     { printf "%s%s", delim, $1;
       delim = ",&"
     }
     END { print "" }' $data   

awk 'BEGIN {
          print ""
          type["%d"] = "int";
          type["%ld"] = "long";
          type["%u"] = "unsigned";
          type["%lu"] = "unsigned long";
          type["%llu"] = "unsigned long long";
	  type["%c"] = "char"
	  type["%s"] = "char *"
	  printf "#define STAT_SCAN_DCL"
     }
     { printf "\\\n %s %s;", type[$2], $1 }
     END { print "" }' $data   

awk 'BEGIN {
          print ""
     }
     END {
          print "#define STAT_SCAN_COUNT " NR
     }' $data

#

rm $data

