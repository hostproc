/* hostproc - SNMP agent for monitoring detailed list of running processes 
   Copyright (C) 2020-2022 Sergey Poznyakoff

   Hostproc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Hostproc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with hostproc. If not, see <http://www.gnu.org/licenses/>. */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <limits.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <ctype.h>
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include "hostproc.h"

static inline PROCESS *
process_alloc(void)
{
	return calloc(1, sizeof(PROCESS));
}

static void
process_free(PROCESS *proc)
{
	free(proc->comm);
	free(proc->cmdline);
	free(proc->exe);
	free(proc);
}

void
proclist_insert(PROCLIST *pl, PROCESS *proc)
{
	PROCESS *l, *m;

	if (TAILQ_EMPTY(&pl->list)) {
		l = NULL;
	} else {
		if (TAILQ_FIRST(&pl->list)->pid > proc->pid) {
			l = NULL;
		} else if (TAILQ_LAST(&pl->list, process_list)->pid < proc->pid) {
			l = TAILQ_LAST(&pl->list, process_list);
		} else {
			size_t count = pl->count;
			l = TAILQ_FIRST(&pl->list);
			while (count > 1) {
				size_t i, n;
				
				n = count / 2;

				for (i = 0, m = l; i < n && m;
				     m = TAILQ_NEXT(m, link), i++)
					;

				if (m->pid < proc->pid) {
					l = m;
					count -= n;
				} else {
					count = n;
				}
			}
		}
	}

	if (l && (m = TAILQ_NEXT(l, link)) != NULL && m->next->pid < proc->pid)
		l = m;
	if (l == NULL)
		TAILQ_INSERT_HEAD(&pl->list, proc, link);
	else
		TAILQ_INSERT_AFTER(&pl->list, l, proc, link);
	pl->count++;
}

PROCESS *
proclist_find_pid(PROCLIST *pl, pid_t pid)
{
	PROCESS *l, *m, *proc = NULL;	
	size_t count = pl->count;

	if (TAILQ_EMPTY(&pl->list))
		return NULL;
	
	if (TAILQ_FIRST(&pl->list)->pid > pid ||
	    TAILQ_LAST(&pl->list, process_list)->pid < pid)
		return NULL;
	
	l = TAILQ_FIRST(&pl->list);
	while (count > 0) {
		size_t i, n;
				
		n = count / 2;

		for (i = 0, m = l; i < n && m; m = TAILQ_NEXT(m, link), i++)
			;

		if (m->pid == pid) {
			proc = m;
			break;
		} else if (n == 0) {
			break;
		} else if (m->pid < pid) {
			l = m;
			count -= n;
		} else {
			count = n;
		}
	}

	return proc;
}

void
proclist_free(PROCLIST *pl)
{
	PROCESS *proc = TAILQ_FIRST(&pl->list);
	while (proc) {
		PROCESS *next = TAILQ_NEXT(proc, link);
		process_free(proc);
		proc = next;
	}
	TAILQ_INIT(&pl->list);
	pl->count  = 0;
}
	
PROCLIST proclist = PROCLIST_INITIALIZER(proclist);

//FIXME
void
proctab_free(void)
{
	proclist_free(&proclist);
}

//FIXME
void
proctab_destroy(void)
{
	proctab_free();
}

static inline int
isnumstr(char const *str)
{
	return str[strspn(str, "0123456789")] == 0;
}

FILE *
fopenat_ro(int dirfd, char const *filename)
{
	int fd;
	FILE *fp;

	fd = openat(dirfd, filename, O_RDONLY);
	if (fd == -1)
		return NULL;
	fp = fdopen(fd, "r");
	if (!fp) {
		int ec = errno;
		close(fd);
		errno = ec;
	}
	return fp;
}

enum {
	GET_OK,
	GET_REMOVED,
	GET_FAILURE
};

static int
read_uid(int dirfd, PROCESS *pp)
{
	FILE *fp;
#define UID_FOUND 0x1
#define GID_FOUND 0x2
#define ALL_FOUND (UID_FOUND | GID_FOUND)
	int found = 0;
	
	fp = fopenat_ro(dirfd, PID_STATUS_FILE);
	if (!fp) {
		if (errno == ENOENT)
			return GET_REMOVED;
		snmp_log(LOG_ERR, "can't open %s/%d/%s: %s\n",
			 PROC_DIR, pp->pid, PID_STATUS_FILE,
			 strerror(errno));
		return GET_FAILURE;
	}
	
	while ((found & ALL_FOUND) != ALL_FOUND && !feof(fp) && !ferror(fp)) {
		if (!(found & UID_FOUND)
		    && fscanf(fp, "Uid: %lu", &pp->uid) == 1)
			found |= UID_FOUND;
		else if (!(found & GID_FOUND)
		    && fscanf(fp, "Gid: %lu", &pp->gid) == 1)
			found |= GID_FOUND;
		else {
			int c;
			while ((c = fgetc(fp)) != EOF && c != '\n')
				;
		}
	}
	if (ferror(fp)) {
		snmp_log(LOG_ERR, "%s/%d/%s: error reading: %s\n",
			 PROC_DIR, pp->pid, PID_STATUS_FILE,
			 strerror(errno));
		fclose(fp);
		return GET_FAILURE;
	}
	fclose(fp);
	if ((found & ALL_FOUND) != ALL_FOUND) {
		snmp_log(LOG_ERR, "%s/%d/%s: bad format\n",
			 PROC_DIR, pp->pid, PID_STATUS_FILE);
		return GET_FAILURE;
	}
	return GET_OK;
}

static int
quote_single(char **pbuf, size_t *pbufsize, size_t *pbuflen,
	     size_t off, size_t len)
{
	char *buf = *pbuf;
	size_t bufsize = *pbufsize;
	size_t buflen = *pbuflen;
	int rc = 0;
	
	if (buflen + 2 >= bufsize) {
		char *p = a2nrealloc(buf, &bufsize, 1);
		if (!p)
			return -1;
		buf = p;
		buflen += 2;
	}	
	memmove(buf + off + 1, buf + off, buflen - off);
	buf[off++] = '\'';
	buflen++;
	memmove(buf + off + len + 1, buf + off + len, buflen - off - len);
	buf[off + len] = '\'';
	buflen++;
	while (len) {
		size_t n;
		char *p;

		p = memchr(buf + off, '\'', len);
		if (!p)
			break;
		
		n = p - buf - off;
		if (n >= len)
			break;
		
		if (buflen + 3 >= bufsize) {
			char *p = a2nrealloc(buf, &bufsize, 1);
			if (!p) {
				rc = -1;
				break;
			}
			buf = p;
		}
		
		off += n;

		memmove(buf + off + 3, buf + off, buflen - off);

		buf[off++] = '\'';
		buf[off++] = '\\';
		buf[off++] = '\'';
		off++;
		len -= n + 1;
		buflen += 3;
	}
	*pbuf = buf;
	*pbufsize = bufsize;
	*pbuflen = buflen;
	return rc;
}

static int
read_cmdline(int dirfd, PROCESS *pp)
{
	int fd;
	char *buf = NULL;
	size_t size = 0;
	size_t total = 0;
	int rc;
	char *p;
	
	fd = openat(dirfd, PID_CMDLINE_FILE, O_RDONLY);
	if (fd == -1) {
		if (errno == ENOENT)
			return GET_REMOVED;
		snmp_log(LOG_ERR, "can't open %s/%d/%s: %s\n",
			 PROC_DIR, pp->pid, PID_CMDLINE_FILE,
			 strerror(errno));
		return GET_FAILURE;
	}

	rc = GET_OK;
	while (1) {
		int n;
		
		p = a2nrealloc(buf, &size, 1);
		if (p == NULL) {
			snmp_log(LOG_ERR, "%s/%d/%s: %s\n",
				 PROC_DIR, pp->pid, PID_CMDLINE_FILE,
				 strerror(errno));
			rc = GET_FAILURE;
			break;
		}
		buf = p;
		
		n = read(fd, buf + total, size - total);
		if (n == 0) {
			buf[total++] = 0;
			break;
		}
		if (n == -1) {
			snmp_log(LOG_ERR, "%s/%d/%s: %s\n",
				 PROC_DIR, pp->pid, PID_CMDLINE_FILE,
				 strerror(errno));
			rc = GET_FAILURE;
			break;
		}
		total += n;
	}

	if (rc == GET_OK) {
		size_t off = 0;
		
		while (off + 1 < total) {
			size_t len;

			if (off > 0)
				buf[off++] = ' ';
			len = strlen(buf + off);
			if (len == 0)
				break;
			if (buf[off + strcspn(buf + off, " \t\'")]) {
				if (quote_single(&buf, &size, &total, off,
						 len)) {
					snmp_log(LOG_ERR, "%s/%d/%s: %s\n",
						 PROC_DIR, pp->pid,
						 PID_CMDLINE_FILE,
						 strerror(errno));
					rc = GET_FAILURE;
					goto end;
				}
			}

			off += strlen(buf + off);
		}
		/* Trim terminating \0 and whitespace */
		total = strlen(buf);
		while (total > 0 && buf[total-1] == ' ')
                        buf[--total] = 0;
	}
end:
	if (rc == GET_OK) {
		if ((p = realloc(buf, total + 1)) != NULL)
			buf = p;
		pp->cmdline = buf;
	} else 
		free(buf);
	close(fd);
	return rc;
}

static int
read_exe(int dirfd, PROCESS *pp)
{
	ssize_t n;
	char *buf = NULL;
	size_t bufsize = 0;
	int rc = GET_OK;
	
	while (1) {
		char *p = a2nrealloc(buf, &bufsize, 1);
		if (p == NULL) {
			snmp_log(LOG_ERR, "%s/%d/%s: %s\n",
				 PROC_DIR, pp->pid, PID_EXE_FILE,
				 strerror(errno));
			rc = GET_FAILURE;
			break;
		}
		buf = p;		
		n = readlinkat(dirfd, PID_EXE_FILE, buf, bufsize);
		if (n < 0) {
			switch (errno) {
			case ERANGE:
				continue;
			case EACCES:
				free(buf);
				buf = NULL;
				break;
			case ENOENT:
				free(buf);
				buf = NULL;
				rc = GET_REMOVED;
				break;
			default:
				snmp_log(LOG_ERR, "readlink(%s/%d/%s): %s\n",
					 PROC_DIR, pp->pid, PID_EXE_FILE,
					 strerror(errno));
				rc = GET_FAILURE;
			}
			break;
		}
		if (n < bufsize) {
			buf[n++] = 0;
			break;
		}
	}
	if (rc == GET_OK)
		pp->exe = buf;
	else
		free(buf);
	return rc;
}

unsigned long
uptime(void)
{
	FILE *fp;
	double up, idle;

	fp = fopen(UPTIME_FILE, "r");
	if (!fp)
		abend("can't open %s: %s", UPTIME_FILE, strerror(errno));
	if (fscanf(fp, "%lf %lf", &up, &idle) != 2)
		abend("%s: wrong format", UPTIME_FILE);
	fclose(fp);
	return up;
}

unsigned long seconds_since_boot;

/* Read /proc/N/stat file FP.  On success, fill ST and return 0.
   If run out of memory, return -1.
   If unable to parse the file, return 1. */
int
proc_stat_read(FILE *fp, struct process_stat *st)
{
	int c, nst, len, rc = 1;
	char pbuf[128];
	
	do {
		if (fscanf(fp, "%d", &st->pid) != 1)
			break;
		while ((c = fgetc(fp)) != EOF && isspace(c))
			;
		if (c != '(')
			break;

		nst = 1;
		len = 0;
		
		while ((c = fgetc(fp)) != EOF) {
			if (c == '(')
				nst++;
			else if (c == ')') {
				if (--nst == 0)
					break;
			} else if (c == '\n')
				break;
			if (len < sizeof(pbuf))
				pbuf[len++] = c;
		}
		if (c != ')')
			break;
		rc = 0;
	} while (0);

	if (rc == 0) {
		if (fscanf(fp, STAT_SCAN_FMT, STAT_SCAN_ARGS(st)) != STAT_SCAN_COUNT) {
			rc = 1;
		} else {
			st->comm = malloc(len + 1);
			if (!st->comm)
				return -1;
			memcpy(st->comm, pbuf, len);
			st->comm[len] = 0;
		}
	}
	
	return rc;
}

static int
procread(int procfd, char *pidstr, PROCLIST *pl, PROCLIST *old)
{
	int dirfd;
	FILE *fp;
	int rc = GET_OK;
	struct process_stat st;
	PROCESS *pp, *prev;

	pp = process_alloc();
	if (!pp) {
		snmp_log(LOG_ERR, "procread %s: %s\n", pidstr,
			 strerror(errno));
		return GET_FAILURE;
	}
	
	dirfd = openat(procfd, pidstr, O_RDONLY | O_NONBLOCK | O_DIRECTORY);
	if (dirfd == -1) {
		process_free(pp);
		if (errno == ENOENT)
			return GET_REMOVED;
		else {
			snmp_log(LOG_ERR, "can't open %s/%s: %s\n",
				 PROC_DIR, pidstr, strerror(errno));
			return GET_FAILURE;
		}
	}

	fp = fopenat_ro(dirfd, PID_STAT_FILE);
	if (!fp) {
		snmp_log(LOG_ERR, "can't open %s/%s/%s: %s\n", PROC_DIR, pidstr,
			 PID_STAT_FILE, strerror(errno));
		rc = GET_FAILURE;
		goto end;
	}

	rc = proc_stat_read(fp, &st);
	fclose(fp);

	if (rc) {
		snmp_log(LOG_ERR, "%s/%s/%s: %s\n",
			 PROC_DIR, pidstr, PID_STAT_FILE,
			 (rc == 1) ? "bad input line"
			           : "out of memory reading file");
		rc = GET_FAILURE;
		goto end;
	}

	
	pp->pid = st.pid;
	pp->ppid = st.ppid;
	pp->time = (st.stime + st.utime) / Hertz;
	pp->state = st.state;
	pp->nice = st.nice;
	pp->priority = st.priority;
	pp->vsize = st.vsize / 1024;
	pp->rss = st.rss * page_size;
	pp->comm = st.comm;

	/* Compute cpu usage */
	if ((unsigned long long)seconds_since_boot >= (st.starttime / Hertz)) {
		pp->runtime = (unsigned long long) seconds_since_boot -
		           	                    (st.starttime / Hertz);
	} else 
		pp->runtime = 0;

	if ((prev = proclist_find_pid(old, pp->pid)) != NULL)
		pp->cpu_inst = (pp->time - prev->time) * 100ULL / ps_interval;
	else
		pp->cpu_inst = 0;
	
	if (pp->runtime)
		pp->cpu_avg = pp->time * 100ULL / pp->runtime;
	else
		pp->cpu_avg = 0;
	
	/* UID */
	rc = read_uid(dirfd, pp);
	if (rc != GET_OK)
		goto end;

	/* Command line */
	rc = read_cmdline(dirfd, pp);
	if (rc != GET_OK)
		goto end;

	rc = read_exe(dirfd, pp);

end:
	if (rc == GET_OK)
		proclist_insert(pl, pp);
	else
		process_free(pp);
	close(dirfd);
	return rc;
}

static void
build_ancestor_list(PROCESS *p)
{
	while ((p->parent = proclist_find_pid(&proclist, p->ppid)) != NULL) {
		p = p->parent;
	}
}

static int
is_ancestor_of(PROCESS *ancestor, PROCESS *proc)
{
	for (; proc; proc = proc->parent) {
		if (proc->parent == ancestor)
			return 1;
	}
	return 0;
}

int
proctab_scan(void)
{
	int fd;
	DIR *dir;
	struct dirent *ent;
	int rc = 0;
	PROCESS *pp;
	PROCLIST prevlist;

	PROCLIST_MOVE(prevlist, proclist);

	fd = open(PROC_DIR, O_RDONLY | O_NONBLOCK | O_DIRECTORY);
	if (fd == -1 || (dir = fdopendir(fd)) == NULL)
		abend("can't open %s: %s", PROC_DIR, strerror(errno));

	seconds_since_boot = uptime();
	
	while ((ent = readdir(dir)) != NULL) {
		if (isnumstr(ent->d_name)) {
			if (procread(fd, ent->d_name, &proclist, &prevlist) == GET_FAILURE)
				break;
		}
	}
	closedir(dir);

	TAILQ_FOREACH(pp, &proclist.list, link) {
		if (!pp->parent)
			build_ancestor_list(pp);
	}

	proclist_free(&prevlist);
	
	return rc;
}

typedef struct ancestor {
	PROCESS *proc;
	PROCGROUP *pgroup;
	STAILQ_ENTRY(ancestor) next;
} ANCESTOR;

typedef	STAILQ_HEAD(, ancestor) ANCESTOR_LIST;

static ANCESTOR *
ancestor_new(ANCESTOR_LIST *alist, PROCESS *proc, PROCGROUP *pgroup)
{
	ANCESTOR *ap = malloc(sizeof(*ap));
	if (ap) {
		ap->proc = proc;
		ap->pgroup = pgroup;
		STAILQ_INSERT_TAIL(alist, ap, next);
	}
	return ap;
}

static void
ancestor_free(ANCESTOR_LIST *alist)
{
	ANCESTOR *ap = STAILQ_FIRST(alist);
	while (ap) {
		ANCESTOR *next = STAILQ_NEXT(ap, next);
		free(ap);
		ap = next;
	}
}

int
proctab_sieve(PROCGROUP_LIST *pgrouplist)
{
	ANCESTOR_LIST ancestor = STAILQ_HEAD_INITIALIZER(ancestor);
	PROCESS *pp;
	PROCGROUP *pgroup;

	if (!pgrouplist || PROCGROUP_LIST_EMPTY(pgrouplist))
		return 0;
	/* Reset pattern list counters */
	PROCGROUP_LIST_FOREACH(pgroup, pgrouplist)
		procgroup_stat_init(pgroup);

	/* Build ancestors list */
	TAILQ_FOREACH(pp, &proclist.list, link) {
		for (pgroup = procgroup_list_match_first(pp, pgrouplist);
		     pgroup;
		     pgroup = procgroup_match_next(pp, pgroup)) {
			if (!ancestor_new(&ancestor, pp, pgroup)) {
				ancestor_free(&ancestor);
				return -1;
			}
		}
	}
	
	if (!STAILQ_EMPTY(&ancestor)) {
		/* Mark all processes that match the ancestor list and update
		   the corresponding exclusion group statistics. */
		for (pp = TAILQ_FIRST(&proclist.list); pp; ) {
			PROCESS *next = TAILQ_NEXT(pp, link);
			ANCESTOR *ap;
			STAILQ_FOREACH(ap, &ancestor, next) {
				if (is_ancestor_of(ap->proc, pp)) {
					procgroup_stat_update(ap->pgroup, pp);
					pp->mask = 1;
				}
			}
			pp = next;
		}

		/* Free the ancestor list */
		ancestor_free(&ancestor);
	}
	return 0;
}

static pthread_mutex_t proclist_mutex = PTHREAD_MUTEX_INITIALIZER;
static unsigned long meminfo[MEMINFO_NFIELDS];

static void
meminfo_read(void)
{
	FILE *fp;
	static char const *varmap[] = {
		"MemTotal",
		"MemAvailable",
		"MemFree",
		"SwapTotal",
		"SwapFree",
	};

	fp = fopen(MEMINFO_FILE, "r");
	if (!fp) {
		abend("can't open %s: %s", MEMINFO_FILE, strerror(errno));
	}

	while (!feof(fp) && !ferror(fp)) {
		char *key;
		unsigned long val;
		int i;
		
		switch (fscanf(fp, "%m[^:]: %lu kB\n", &key, &val)) {
		case 2:
			for (i = 0; i < sizeof(varmap)/sizeof(varmap[0]); i++)
				if (strcmp(varmap[i], key) == 0) {
					meminfo[i] = val;
					break;
				}
			free(key);
			break;
		case 1:
			free(key);
		default:
			while ((i = fgetc(fp)) != EOF && i != '\n')
				;
		}
	}
	if (ferror(fp)) {
		snmp_log(LOG_ERR, "%s: error reading: %s\n",
			 MEMINFO_FILE,
			 strerror(errno));
	}
	fclose(fp);
}

unsigned long
meminfo_get(int i)
{
	unsigned long r;
	pthread_mutex_lock(&proclist_mutex);
	r = meminfo[i];
	pthread_mutex_unlock(&proclist_mutex);
	return r;
}

int
proctab_foreach(int (*handler)(PROCESS *, void *), void *data)
{
	int rc = 0;
	PROCESS *pp;
	pthread_mutex_lock(&proclist_mutex);
	TAILQ_FOREACH(pp, &proclist.list, link) {
		if ((rc = handler(pp, data)) != 0)
			break;
	}
	pthread_mutex_unlock(&proclist_mutex);
	return rc;
}

void *
hostproc_thr_ps(void *arg)
{
	while (1) {
		struct timespec ts;

		clock_gettime(CLOCK_MONOTONIC, &ts);

		pthread_mutex_lock(&proclist_mutex);
		meminfo_read();
		proctab_scan();
		PROCGROUP_LIST_LOCK(&exclude_groups);
		proctab_sieve(&exclude_groups);
		PROCGROUP_LIST_UNLOCK(&exclude_groups);
		table_invalidate();
		pthread_mutex_unlock(&proclist_mutex);
		
		ts.tv_sec += ps_interval;
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &ts, NULL);
	}
}
