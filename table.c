/* hostproc - SNMP agent for monitoring detailed list of running processes 
   Copyright (C) 2020-2022 Sergey Poznyakoff

   Hostproc is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Hostproc is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with hostproc. If not, see <http://www.gnu.org/licenses/>. */
#include <stdlib.h>
#include "hostproc.h"
#include "hostproc_mib.h"

enum {
	PROCESS_TABLE_VALID = 0x01,
	PROCESSGROUP_TABLE_VALID = 0x02,
	EXCLUDED_TABLE_VALID = 0x04
};

static int table_validity_state;
static pthread_mutex_t table_validity_mutex = PTHREAD_MUTEX_INITIALIZER;

static int
table_validity_update(int what)
{
	int result;
	pthread_mutex_lock(&table_validity_mutex);
	result = table_validity_state & what;
	table_validity_state |= what;
	pthread_mutex_unlock(&table_validity_mutex);
	return result;
}

void
table_invalidate(void)
{
	pthread_mutex_lock(&table_validity_mutex);
	table_validity_state = 0;
	pthread_mutex_unlock(&table_validity_mutex);
}

static int
processTable_load_row(PROCESS *pp, void *data)
{
	netsnmp_tdata *table = data;
	netsnmp_tdata_row *data_row;
	struct processTable_entry *ent;

	if (pp->mask)
		return SNMP_ERR_NOERROR;
	
	ent = SNMP_MALLOC_TYPEDEF(struct processTable_entry);
	if (!ent) 
		return SNMP_ERR_GENERR;

	memset(ent, 0, sizeof(*ent));
	ent->processIndex = pp->pid;
	ent->processPID = pp->pid;
	ent->processPPID = pp->ppid;
	ent->processUID = pp->uid;
	ent->processGID = pp->gid;
	ent->processCPUAvg = pp->cpu_avg;
	ent->processCPUInst = pp->cpu_inst;
	ent->processTime = pp->time * 100;

	if ((ent->processState = malloc(2)) == NULL)
		goto err;
	ent->processState[0] = pp->state;
	ent->processState[1] = 0;	
	ent->processState_len = 1;
	
	ent->processNice = pp->nice;
	ent->processPriority = pp->priority;
	ent->processVSZ = pp->vsize;
	ent->processRSS = pp->rss;

	if ((ent->processComm = strdup(pp->comm)) == NULL)
		goto err;
	ent->processComm_len = strlen(ent->processComm);

	if ((ent->processCmdline = strdup(pp->cmdline)) == NULL)
		goto err;
	ent->processCmdline_len = strlen(ent->processCmdline);

	if (pp->exe) {
		if ((ent->processExe = strdup(pp->exe)) == NULL)
			goto err;
		ent->processExe_len = strlen(ent->processExe);
	} else {
		ent->processExe_len = ent->processComm_len + 2;
		if ((ent->processExe = malloc(ent->processExe_len + 1)) == NULL)
			goto err;
		ent->processExe[0] = '[';
		strcpy(ent->processExe + 1, ent->processComm);
		strcat(ent->processExe, "]");
	}
	data_row = netsnmp_tdata_create_row();
	if (!data_row)
		goto err;
	data_row->data = ent;
	netsnmp_tdata_row_add_index(data_row, ASN_INTEGER,
				    &ent->processIndex,
				    sizeof(ent->processIndex));
	netsnmp_tdata_add_row(table, data_row);

	return SNMP_ERR_NOERROR;

err:
	processTable_entry_free(ent);
	SNMP_FREE(ent);
	return SNMP_ERR_GENERR;
}

int
processTable_load(netsnmp_cache *cache, void *vmagic)
{
	netsnmp_tdata *table = (netsnmp_tdata *) vmagic;
	
	if (!table_validity_update(PROCESS_TABLE_VALID)) {
		int rc;
		processTable_free(cache, vmagic);
		if ((rc = proctab_foreach(processTable_load_row, table)) != SNMP_ERR_NOERROR)
			return rc;
	}
	return 0;
}

void
processTable_entry_free(void *data)
{
	struct processTable_entry *ent = data;
	free(ent->processState);
	free(ent->processComm);
	free(ent->processCmdline);
	free(ent->processExe);
}

static void
proc_counter_init(void)
{
	PROCGROUP *p;

	PROCGROUP_LIST_FOREACH(p, &process_groups)
		procgroup_stat_init(p);
}

static int
proc_counter(PROCESS *pp, void *data)
{
	PROCGROUP *pgr;

	if (pp->mask)
		return 0;
	PROCGROUP_LIST_LOCK(&process_groups);
	for (pgr = procgroup_list_match_first(pp, &process_groups);
	     pgr;
	     pgr = procgroup_match_next(pp, pgr)) {
		procgroup_stat_update(pgr, pp);
	}
	PROCGROUP_LIST_UNLOCK(&process_groups);
	return 0;
}

static int
proc_counter_load(netsnmp_tdata *table)
{
	size_t i;
	PROCGROUP *pgr;
	int ret = SNMP_ERR_NOERROR;
	
	i = 0;
	PROCGROUP_LIST_LOCK(&process_groups);
	PROCGROUP_LIST_FOREACH(pgr, &process_groups) {
		struct processGroupTable_entry *ent;
		netsnmp_tdata_row *data_row;
	
		ent = SNMP_MALLOC_TYPEDEF(struct processGroupTable_entry);
		if (!ent) {
			ret = SNMP_ERR_GENERR;
			break;
		}
		ent->processGroupIndex = i++;
		ent->processGroupName = pgr->name;
		ent->processGroupName_len = strlen(ent->processGroupName);

		ent->processGroupPattern = pgr->pattern;
		ent->processGroupPattern_len = strlen(ent->processGroupPattern);
		ent->processGroupField = pgr->field;
		ent->processGroupMatch = pgr->match;
		
		ent->processGroupMinCount = pgr->min_count;
		ent->processGroupMaxCount = pgr->max_count;
		ent->processGroupMaxVSZ = pgr->vsize_max;
		ent->processGroupMaxRSS = pgr->rss_max;
		ent->processGroupMaxCPU = pgr->cpu_max;
		
		ent->processGroupCount = pgr->count;
		ent->processGroupCPUMax = pgr->cpu_inst_max;
		if (pgr->count > 0) {
			ent->processGroupCPUAvg = pgr->cpu_avg_sum / pgr->count;
			ent->processGroupCPUInst = pgr->cpu_inst_sum / pgr->count;
		} else {
			ent->processGroupCPUAvg = 0;
			ent->processGroupCPUInst = 0;
		}
		ent->processGroupVSZ = pgr->vsize;
		ent->processGroupRSS = pgr->rss;
		ent->processGroupOK = procgroup_ok_status(pgr);

		data_row = netsnmp_tdata_create_row();
		if (!data_row) {
			processGroupTable_entry_free(ent);
			SNMP_FREE(ent);
			ret = SNMP_ERR_GENERR;
			break;
		}
		
		data_row->data = ent;
		netsnmp_tdata_row_add_index(data_row, ASN_INTEGER,
					    &ent->processGroupIndex,
					    sizeof(ent->processGroupIndex));
		netsnmp_tdata_add_row(table, data_row);
	}
	PROCGROUP_LIST_UNLOCK(&process_groups);
	return ret;
}

int
processGroupTable_load(netsnmp_cache *cache, void *vmagic)
{
	netsnmp_tdata *table = (netsnmp_tdata *) vmagic;
	if (!table_validity_update(PROCESSGROUP_TABLE_VALID)) {
		proc_counter_init();
		proctab_foreach(proc_counter, NULL);
		processGroupTable_free(cache, vmagic);
		proc_counter_load(table);
	}
	return SNMP_ERR_NOERROR;
}

void
processGroupTable_entry_free(void *data)
{
	/* nothing */
}

/* FIXME: This largely copies proc_counter_load. */
static int
excl_counter_load(netsnmp_tdata *table)
{
	size_t i;
	PROCGROUP *pgr;
	int ret = SNMP_ERR_NOERROR;

	i = 0;
	PROCGROUP_LIST_LOCK(&exclude_groups);
	PROCGROUP_LIST_FOREACH(pgr, &exclude_groups) {
		struct processExcludedTable_entry *ent;
		netsnmp_tdata_row *data_row;
	
		ent = SNMP_MALLOC_TYPEDEF(struct processExcludedTable_entry);
		if (!ent) {
			ret = SNMP_ERR_GENERR;
			break;
		}
		ent->processExcludedIndex = i++;

		ent->processExcludedName = pgr->name;
		ent->processExcludedName_len = strlen(ent->processExcludedName);

		ent->processExcludedPattern = pgr->pattern;
		ent->processExcludedPattern_len = strlen(ent->processExcludedPattern);
		ent->processExcludedField = pgr->field;
		ent->processExcludedMatch = pgr->match;

		ent->processExcludedMinCount = pgr->min_count;
		ent->processExcludedMaxCount = pgr->max_count;
		ent->processExcludedMaxVSZ = pgr->vsize_max;
		ent->processExcludedMaxRSS = pgr->rss_max;
		ent->processExcludedMaxCPU = pgr->cpu_max;

		ent->processExcludedCount = pgr->count;
		ent->processExcludedCPUMax = pgr->cpu_inst_max;
		if (pgr->count > 0) {
			ent->processExcludedCPUAvg = pgr->cpu_avg_sum / pgr->count;
			ent->processExcludedCPUInst = pgr->cpu_inst_sum / pgr->count;
		} else {
			ent->processExcludedCPUAvg = 0;
			ent->processExcludedCPUInst = 0;
		}
		ent->processExcludedVSZ = pgr->vsize;
		ent->processExcludedRSS = pgr->rss;
		ent->processExcludedOK = procgroup_ok_status(pgr);
		
		data_row = netsnmp_tdata_create_row();
		if (!data_row) {
			processExcludedTable_entry_free(ent);
			SNMP_FREE(ent);
			ret = SNMP_ERR_GENERR;
			break;
		}
		
		data_row->data = ent;
		netsnmp_tdata_row_add_index(data_row, ASN_INTEGER,
					    &ent->processExcludedIndex,
					    sizeof(ent->processExcludedIndex));
		netsnmp_tdata_add_row(table, data_row);
	}
	PROCGROUP_LIST_UNLOCK(&exclude_groups);	
	return ret;
}

int
processExcludedTable_load(netsnmp_cache *cache, void *vmagic)
{
	netsnmp_tdata *table = (netsnmp_tdata *) vmagic;
	if (!table_validity_update(EXCLUDED_TABLE_VALID)) {
		processExcludedTable_free(cache, vmagic);
		excl_counter_load(table);
	}
	return SNMP_ERR_NOERROR;
}

void
processExcludedTable_entry_free(void *data)
{
	/* nothing */
}

